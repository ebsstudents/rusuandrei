﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TelephoneAgenda
{
    public class Contact
    {
      
      string allContacts;
      string[] contactStrings;

      public string Name { get; set; }
      public string Telephone { get; set; }

      public Contact() {
          Name = "";
          Telephone = "";
      }

      public Contact(string name,string telephone) {
          this.Name = name;
          this.Telephone = telephone;
      }

      public void InitialiseDatabaseFromInitialDatabase() {
          allContacts = System.IO.File.ReadAllText(@"D:\Facultate\An 3 - semestrul 2\EBS - Copy\TelephoneAgenda\TelephoneAgenda\input.txt");
          char[] delimiterCharsForContact = { '.' };
          contactStrings = allContacts.Split(delimiterCharsForContact);      
      }

      public void InitialiseDatabaseFromNewDatabase()
      {
          char[] delimiterCharsForContact = { '.' };
          contactStrings = allContacts.Split(delimiterCharsForContact);
      }

      public void AddContactsToGridFromFile(DataGridView TableForNamesAndPhones, bool initialise) {
          if (initialise)
          {
              InitialiseDatabaseFromInitialDatabase();
          }
          else {
              InitialiseDatabaseFromNewDatabase();
          }

          char[] delimiterCharsForNumbers = { ';' };
          foreach (string currentContact in contactStrings)
          {
              if (currentContact != null)
              {
                  string[] nameAndTelephoneConcatenated = currentContact.Split(delimiterCharsForNumbers);
                  TableForNamesAndPhones.Rows.Add(nameAndTelephoneConcatenated[0], nameAndTelephoneConcatenated[1]);
              }
          }
      }

      public void UpdateDatabase(DataGridView TableForNamesAndPhones) {
          allContacts = null;
          TableForNamesAndPhones.Refresh();
          for (int currentRowId = 0; currentRowId < TableForNamesAndPhones.Rows.Count-1; currentRowId++) {     
                  allContacts = allContacts + TableForNamesAndPhones.Rows[currentRowId].Cells[0].Value.ToString() + ";" +
                                TableForNamesAndPhones.Rows[currentRowId].Cells[1].Value.ToString() + ".";
              
          }
          allContacts = allContacts.Remove(allContacts.Length - 1);
          System.IO.File.WriteAllText(@"D:\Facultate\An 3 - semestrul 2\EBS - Copy\TelephoneAgenda\TelephoneAgenda\input.txt", allContacts);
          TableForNamesAndPhones.Refresh();
      }

      public int[] GetIndexesForSelectedRows(ref int numberOfIndexes, DataGridView TableForNamesAndPhones)
      {
          int[] indexesForSelectedRows = new int[20];
          foreach (DataGridViewCell selectedCell in TableForNamesAndPhones.SelectedCells)
          {
              if (selectedCell.Selected)
              {
                  indexesForSelectedRows[numberOfIndexes++] = selectedCell.RowIndex;

              }
          }
          return indexesForSelectedRows;
      }

      public void DeleteNameAndPhoneFromTable(DataGridView TableForNamesAndPhones)
      {
          int numberOfIndexes = 0;
          int[] indexesForSelectedRows = GetIndexesForSelectedRows(ref numberOfIndexes,TableForNamesAndPhones);
          for (int indexThatWillBeRemoved = 0; indexThatWillBeRemoved < numberOfIndexes; indexThatWillBeRemoved++)
          {
              TableForNamesAndPhones.Rows.RemoveAt(indexesForSelectedRows[indexThatWillBeRemoved]);
              UpdateDatabase(TableForNamesAndPhones);
          }
      }

      public void EliberateDataGrid(DataGridView TableForNamesAndPhones) {
          int numberOfRowsInDataGrid = TableForNamesAndPhones.Rows.Count;
          for (int indexThatWillBeRemoved = numberOfRowsInDataGrid-2; indexThatWillBeRemoved >= 0; indexThatWillBeRemoved--)
          {
              TableForNamesAndPhones.Rows.RemoveAt(indexThatWillBeRemoved);
          }
      }

      public void AddNameAndPhoneToTable(DataGridView TableForNamesAndPhones, string Name, string Telephone)
      {
          if (validName(Name) && validTelephoneNumber(Telephone))
          {
              TableForNamesAndPhones.Rows.Add(Name, Telephone);
              UpdateDatabase(TableForNamesAndPhones);
          }
          else
              MessageBox.Show("Invalid Name/Telephone (Name needs to contain only letters and Telephone only numbers");
      }

      public void UpdateNameAndPhoneInTable(DataGridView TableForNamesAndPhones, string Name, string Telephone)
      {

          if (validName(Name) && validTelephoneNumber(Telephone))
          {
              int numberOfIndexes = 0;
              int[] indexesForSelectedRows = GetIndexesForSelectedRows(ref numberOfIndexes, TableForNamesAndPhones);
              for (int indexThatWillBeUpdated = 0; indexThatWillBeUpdated < numberOfIndexes; indexThatWillBeUpdated++)
                  TableForNamesAndPhones.Rows[indexesForSelectedRows[indexThatWillBeUpdated]].SetValues(Name, Telephone);
              UpdateDatabase(TableForNamesAndPhones);
          }
          else
          {
              MessageBox.Show("Invalid Name/Telephone (Name needs to contain only letters and Telephone only numbers");
          }
      }

      public void FilterNames(string filterName, DataGridView TableForNamesAndPhones)
      {
        allContacts = null;
        if (filterName != "")
        {
            for (int currentRowId = 0; currentRowId < TableForNamesAndPhones.Rows.Count-1; currentRowId++)
            {
                if (TableForNamesAndPhones.Rows[currentRowId].Cells[0].Value.ToString().Contains(filterName)) {
                    allContacts = allContacts + TableForNamesAndPhones.Rows[currentRowId].Cells[0].Value.ToString() + ";" +
                                  TableForNamesAndPhones.Rows[currentRowId].Cells[1].Value.ToString() + ".";

                }
            }
            if (allContacts != null)
            {
                allContacts = allContacts.Remove(allContacts.Length - 1);
                EliberateDataGrid(TableForNamesAndPhones);
                AddContactsToGridFromFile(TableForNamesAndPhones,false);
            }
            
        }
        else {
            EliberateDataGrid(TableForNamesAndPhones);
            AddContactsToGridFromFile(TableForNamesAndPhones,true);
        }
      }

      public bool validName(string nameToCheck)
      {
          return Regex.IsMatch(nameToCheck, @"^[a-zA-Z\s]+$");
      }

      public bool validTelephoneNumber(string numberToCheck)
      {
          return Regex.IsMatch(numberToCheck, @"^[0-9]+$");    
      }

      
    }
}
