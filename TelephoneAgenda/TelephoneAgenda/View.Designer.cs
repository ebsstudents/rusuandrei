﻿namespace TelephoneAgenda
{
    partial class View
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.TelephoneTextBox = new System.Windows.Forms.TextBox();
            this.TableForNamesAndPhones = new System.Windows.Forms.DataGridView();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelephoneColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameLabel = new System.Windows.Forms.Label();
            this.TelephoneLabel = new System.Windows.Forms.Label();
            this.AddNameAndPhoneToTable = new System.Windows.Forms.Button();
            this.DeleteNameAndPhoneFromTable = new System.Windows.Forms.Button();
            this.UpdateNameAndPhoneInTable = new System.Windows.Forms.Button();
            this.filterButton = new System.Windows.Forms.Button();
            this.filterLabel = new System.Windows.Forms.Label();
            this.FilterTextBox = new System.Windows.Forms.TextBox();
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.nameAndPhonePanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.TableForNamesAndPhones)).BeginInit();
            this.buttonsPanel.SuspendLayout();
            this.nameAndPhonePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(37, 43);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(127, 20);
            this.NameTextBox.TabIndex = 0;
            // 
            // TelephoneTextBox
            // 
            this.TelephoneTextBox.Location = new System.Drawing.Point(37, 95);
            this.TelephoneTextBox.Name = "TelephoneTextBox";
            this.TelephoneTextBox.Size = new System.Drawing.Size(127, 20);
            this.TelephoneTextBox.TabIndex = 1;
            // 
            // TableForNamesAndPhones
            // 
            this.TableForNamesAndPhones.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TableForNamesAndPhones.BackgroundColor = System.Drawing.SystemColors.Control;
            this.TableForNamesAndPhones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TableForNamesAndPhones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameColumn,
            this.TelephoneColumn});
            this.TableForNamesAndPhones.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.TableForNamesAndPhones.Location = new System.Drawing.Point(12, 52);
            this.TableForNamesAndPhones.Name = "TableForNamesAndPhones";
            this.TableForNamesAndPhones.RowHeadersVisible = false;
            this.TableForNamesAndPhones.Size = new System.Drawing.Size(307, 301);
            this.TableForNamesAndPhones.TabIndex = 2;
            this.TableForNamesAndPhones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // NameColumn
            // 
            this.NameColumn.HeaderText = "Nume";
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.Width = 150;
            // 
            // TelephoneColumn
            // 
            this.TelephoneColumn.HeaderText = "Tel";
            this.TelephoneColumn.Name = "TelephoneColumn";
            this.TelephoneColumn.Width = 150;
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(41, 27);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(35, 13);
            this.NameLabel.TabIndex = 3;
            this.NameLabel.Text = "Nume";
            // 
            // TelephoneLabel
            // 
            this.TelephoneLabel.AutoSize = true;
            this.TelephoneLabel.Location = new System.Drawing.Point(41, 79);
            this.TelephoneLabel.Name = "TelephoneLabel";
            this.TelephoneLabel.Size = new System.Drawing.Size(43, 13);
            this.TelephoneLabel.TabIndex = 4;
            this.TelephoneLabel.Text = "Telefon";
            // 
            // AddNameAndPhoneToTable
            // 
            this.AddNameAndPhoneToTable.Location = new System.Drawing.Point(23, 57);
            this.AddNameAndPhoneToTable.Name = "AddNameAndPhoneToTable";
            this.AddNameAndPhoneToTable.Size = new System.Drawing.Size(29, 23);
            this.AddNameAndPhoneToTable.TabIndex = 5;
            this.AddNameAndPhoneToTable.Text = "+";
            this.AddNameAndPhoneToTable.UseVisualStyleBackColor = true;
            this.AddNameAndPhoneToTable.Click += new System.EventHandler(this.AddNameAndPhoneToTable_Click);
            // 
            // DeleteNameAndPhoneFromTable
            // 
            this.DeleteNameAndPhoneFromTable.Location = new System.Drawing.Point(59, 57);
            this.DeleteNameAndPhoneFromTable.Name = "DeleteNameAndPhoneFromTable";
            this.DeleteNameAndPhoneFromTable.Size = new System.Drawing.Size(29, 23);
            this.DeleteNameAndPhoneFromTable.TabIndex = 6;
            this.DeleteNameAndPhoneFromTable.Text = "-";
            this.DeleteNameAndPhoneFromTable.UseVisualStyleBackColor = true;
            this.DeleteNameAndPhoneFromTable.Click += new System.EventHandler(this.DeleteNameAndPhoneFromTable_Click);
            // 
            // UpdateNameAndPhoneInTable
            // 
            this.UpdateNameAndPhoneInTable.Location = new System.Drawing.Point(94, 40);
            this.UpdateNameAndPhoneInTable.Name = "UpdateNameAndPhoneInTable";
            this.UpdateNameAndPhoneInTable.Size = new System.Drawing.Size(60, 56);
            this.UpdateNameAndPhoneInTable.TabIndex = 7;
            this.UpdateNameAndPhoneInTable.Text = "Update selected rows";
            this.UpdateNameAndPhoneInTable.UseVisualStyleBackColor = true;
            this.UpdateNameAndPhoneInTable.Click += new System.EventHandler(this.UpdateNameAndPhoneInTable_Click);
            // 
            // filterButton
            // 
            this.filterButton.Location = new System.Drawing.Point(186, 11);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(56, 23);
            this.filterButton.TabIndex = 8;
            this.filterButton.Text = "Filter";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler(this.filterButton_Click);
            // 
            // filterLabel
            // 
            this.filterLabel.AutoSize = true;
            this.filterLabel.Location = new System.Drawing.Point(12, 19);
            this.filterLabel.Name = "filterLabel";
            this.filterLabel.Size = new System.Drawing.Size(29, 13);
            this.filterLabel.TabIndex = 9;
            this.filterLabel.Text = "Filter";
            // 
            // FilterTextBox
            // 
            this.FilterTextBox.Location = new System.Drawing.Point(55, 13);
            this.FilterTextBox.Name = "FilterTextBox";
            this.FilterTextBox.Size = new System.Drawing.Size(125, 20);
            this.FilterTextBox.TabIndex = 10;
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonsPanel.AutoSize = true;
            this.buttonsPanel.Controls.Add(this.UpdateNameAndPhoneInTable);
            this.buttonsPanel.Controls.Add(this.DeleteNameAndPhoneFromTable);
            this.buttonsPanel.Controls.Add(this.AddNameAndPhoneToTable);
            this.buttonsPanel.Location = new System.Drawing.Point(384, 245);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(179, 107);
            this.buttonsPanel.TabIndex = 11;
            // 
            // nameAndPhonePanel
            // 
            this.nameAndPhonePanel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.nameAndPhonePanel.AutoSize = true;
            this.nameAndPhonePanel.Controls.Add(this.TelephoneLabel);
            this.nameAndPhonePanel.Controls.Add(this.NameLabel);
            this.nameAndPhonePanel.Controls.Add(this.TelephoneTextBox);
            this.nameAndPhonePanel.Controls.Add(this.NameTextBox);
            this.nameAndPhonePanel.Location = new System.Drawing.Point(374, 61);
            this.nameAndPhonePanel.Name = "nameAndPhonePanel";
            this.nameAndPhonePanel.Size = new System.Drawing.Size(188, 127);
            this.nameAndPhonePanel.TabIndex = 12;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 485);
            this.Controls.Add(this.nameAndPhonePanel);
            this.Controls.Add(this.buttonsPanel);
            this.Controls.Add(this.FilterTextBox);
            this.Controls.Add(this.filterLabel);
            this.Controls.Add(this.filterButton);
            this.Controls.Add(this.TableForNamesAndPhones);
            this.Name = "View";
            this.Text = "Agenda";
            this.Load += new System.EventHandler(this.View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TableForNamesAndPhones)).EndInit();
            this.buttonsPanel.ResumeLayout(false);
            this.nameAndPhonePanel.ResumeLayout(false);
            this.nameAndPhonePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox TelephoneTextBox;
        private System.Windows.Forms.DataGridView TableForNamesAndPhones;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label TelephoneLabel;
        private System.Windows.Forms.Button AddNameAndPhoneToTable;
        private System.Windows.Forms.Button DeleteNameAndPhoneFromTable;
        private System.Windows.Forms.Button UpdateNameAndPhoneInTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelephoneColumn;
        private System.Windows.Forms.Button filterButton;
        private System.Windows.Forms.Label filterLabel;
        private System.Windows.Forms.TextBox FilterTextBox;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.Panel nameAndPhonePanel;
    }
}

