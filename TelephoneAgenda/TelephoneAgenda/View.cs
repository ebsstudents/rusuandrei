﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TelephoneAgenda
{
    public partial class View : Form
    {
        public View()
        {
            InitializeComponent();
            contact.AddContactsToGridFromFile(TableForNamesAndPhones,true);
        }

        Contact contact = new Contact();
        List<Contact> contactList = new List<Contact>();

        public void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void DeleteNameAndPhoneFromTable_Click(object sender, EventArgs e)
        {
            contact.DeleteNameAndPhoneFromTable(TableForNamesAndPhones);
        }

        public void AddNameAndPhoneToTable_Click(object sender, EventArgs e)
        {
            contact.AddNameAndPhoneToTable(TableForNamesAndPhones, NameTextBox.Text, TelephoneTextBox.Text);
        }

        public void UpdateNameAndPhoneInTable_Click(object sender, EventArgs e)
        {
            contact.UpdateNameAndPhoneInTable(TableForNamesAndPhones,NameTextBox.Text,TelephoneTextBox.Text);
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            contact.FilterNames(FilterTextBox.Text, TableForNamesAndPhones);
        }

        public string NameFromTextBox {
          get { return this.NameTextBox.Text; }
          set { this.NameTextBox.Text = value; }
        }

        public string TelephoneFromTextBox {
          get { return this.TelephoneTextBox.Text; }
          set { this.TelephoneTextBox.Text = value; }
        }

        private void View_Load(object sender, EventArgs e)
        {

        }

        
    }
}
